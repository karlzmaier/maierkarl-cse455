package com.example.karlz.currencyconverter;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {
    private EditText editText01;
    private Button convbutton;
    private TextView textView01;
    private String usd;

    private static final String url = "https://api.fixer.io/latest?base=USD";

    String json = "";

    String line = "";

    String rate = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText01 = findViewById(R.id.EditText01);
        convbutton = findViewById(R.id.convertbutton);
        textView01 = findViewById(R.id.Yen);

        convbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View convertToYen) {

                BackgroundTask object = new BackgroundTask();

                object.execute();

                //double convert = Double.parseDouble(json);

              /*usd = editText01.getText().toString();

                if (usd.equals("")){
                    textView01.setText("This field cannot be blank!");
                } else {
                    Double dInputs = Double.parseDouble(usd);

                    Double result = dInputs * 112.57;

                    textView01.setText("$" + usd + " = " + "¥"+String.format("%.2f", result));

                    editText01.setText("");
                }*/
            }
        });

    }
    private class BackgroundTask extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() { super.onPreExecute();}
        @Override
        protected void onProgressUpdate(Void... values) {super.onProgressUpdate(values);}
        @Override
        protected void onPostExecute(String result){
            super.onPostExecute(result);

            System.out.println("\nWhat is rate: " + rate + "\n");

            Double value = Double.parseDouble(rate);

            System.out.println("\nTesting JSON String Exchange Rate INSIDE AsyncTask: " + value);

            usd = editText01.getText().toString();

            if (usd.equals("")){
                textView01.setText("This field cannot be blank!");
            } else {
                Double dInputs = Double.parseDouble(usd);

                Double output = dInputs * 112.57;

                textView01.setText("$" + usd + " = " + "¥"+String.format("%.2f", output));

                editText01.setText("");
            }
            //double convert = Double.parseDouble(json);
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                URL web_url = new URL(MainActivity.this.url);

                HttpURLConnection httpURLConnection = (HttpURLConnection) web_url.openConnection();

                httpURLConnection.setRequestMethod("GET");

                System.out.println("\nTESTING ... BEFORE connection method to URL\n");

                httpURLConnection.connect();

                InputStream inputStream = httpURLConnection.getInputStream();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                System.out.println("CONNECTION SUCCESSFUL\n");

                while (line != null) {
                    line = bufferedReader.readLine();

                    json += line;
                }

            }catch (IOException e){
                e.printStackTrace();
            }
            try{
            System.out.println("\nTHE JSON: " + json);

            JSONObject obj = new JSONObject(json);

            JSONObject objRate = obj.getJSONObject("rates");

            rate = objRate.get("JPY").toString();

        }  catch (JSONException e){
                e.printStackTrace();
            }
        return null;

        }

    }
}
